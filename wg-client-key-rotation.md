# WG identity hiding

## Client keys

### Client key states
- unreg
- reg.unused
- reg.used
- dereg

### State transitions
unreg -> reg.unused -> reg.used -> dereg    # normal flow
unreg -> reg.unused -> dereg                # cleanup
unreg -> dereg                              # cleanup

### File names
Key file: <i/f><TS>.<state>
Hard link: $if -> key file # Symlinks would not allow for renaming the key file 

### State transition triggers
#### $if down

test -f $if.*.reg.unused && ln $(first $if.*.reg.unused) $if

#### $if up

basename=$(tspart $(deref $if))
mv $basename.reg.unused $basename.reg.used

ts=$(now)
Generate new key, $now.unreg
Register $now.unreg
On register ack, mv $now.unreg $now.reg.unused

first_unused=$(first $if.*.reg.unused)
$(samefile $first_used $if) ||  mv .reg.used .dereg



## Function definitions

first() {
    match="$1"; shift
    ls -rt "$match" | head -1
}

deref() {
}

stat_compat() {
    format="$1"; shift
    case $(uname) in
        Linux) F=-c ;;
        FreeBSD) F=-f ;;
        *) F=-c ;;
    esac
    stat $F $format $@
}

samefile() {
    [ $(stat_compat %i "$1") -eq $(stat_compat %i "$2") ]
}

now() {
    date -u +%s
}

tspart() {
}


register() {
}

deregister() {
}

## Open questions

### Acknowledgment yes or no?

Messages are sent over TCP over a wg i/f.

Can acks be synchronous on the message layer?

Do we need to wait for acknowledgment of key registration?
Do we need to wait for acknowledgment of key de-registration?

Asynchronous acks complicate the client side implementation.
Messages will arrive given that the i/f keeps up.
