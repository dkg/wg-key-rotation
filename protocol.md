Protocol for WireGuard key rotation
===================================

Server keeps a pool of keys associated with some network configuration
parameters for each client.

The client knows a name and port number for the key rotation protocol.
Typically, this would happen over an active WG interface, but we do
not require it to be limited to that.

The key rotation listener uses a modern version of TLS (1.2 or later)
in a forward-secure mode.  It does not use 0-RTT.

Each message is marked with a 64-bit unsigned counter, which the
client should always increase with each request.  The server SHOULD
track the highest counter submitted by a client, and SHOULD reject
requests with a lower or equal counter.

The cleartext application payload of the client's registration or
de-registration request is (size of fields in octets is represented in
trailing angle brackets):

    {
      Action<1> = 0x00 ("registration") or 0x01 ("deregistration")
      Counter<8>
      SigningKey<32>
      NewKey<32>
    } Request<73>
    Signature("wg-key-rotation" || Request)<32>

The signatures are made with EdDSA.

The server's response is a single octet:

    0x00 -- success, request succeeded
    0x01 -- rejected (no reason specified)
    0x02 -- rejected (implementation failure)
    0x03 -- rejected (counter too low)
    0x04 -- rejected (unacceptable SigningKey)
    0x05 -- rejected (too many registered keys)

At the end of the exchange, the TLS channel should be terminated (no
request pipelining or out-of-order responses).

Timing and defending against error analysis
-------------------------------------------

In the event of an authentication failure, if the server has no reason
to believe that the client is already authenticated (e.g. if it did
not receive the request over a wireguard link that is credentialed to
the same account that matches the SigningKey), then the server SHOULD
reject the request with response 0x01 after waiting a fixed amount of
time (1 second from receipt is reasonable).

Design Rationales
-----------------

This is intentionally a very simple protocol to implement that can
support all steps necessary for key rotation.  It is not the fastest
possible protocol.

We rely on TLS (rather than wireguard itself because:

 * we want forward-secrecy for these requests, and
  
 * we do not know how to ensure that the request will travel only over
   the protected wireguard link, and

 * it's concievable that someone might want to extend this protocol
   for a "bootstrap" step in the future, to handle initial
   registration when no wireguard link is established.

We supply the Counter as a simple anti-replay mechanism.  We went with
this because of easier implementation.  If we wanted stronger
anti-replay protection, we could include a [TLS exporter] [RFC5705]
[RFC8446 7.5] to bind the request to the specific TLS session.

[RFC5705]: https://tools.ietf.org/html/rfc5705
[RFC8446 7.5]: https://tools.ietf.org/html/rfc8446#section-7.5
