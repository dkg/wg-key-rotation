Key Rotation for WireGuard
==========================

[WireGuard](https://wireguard.com) is a modern, simple, opinionated IP
tunneling mechanism.

One security concern with WireGuard is that the first message in any
WireGuard flow is encrypted statically to the recipient's long-term
key.  Since the first message contains proof of the sending peer's
long-term key, the identity of the sending peer is not [Forward
Secret](https://en.wikipedia.org/wiki/Forward_secrecy).  That is, if
an attacker with a historical copy of encrypted network traffic can at
some point in the future obtain the recipient's long-term secret key,
then they can use that information to link the long-term identity of
the sending peer to each flow.

This is particularly problematic in the encrypted Internet proxy
("VPN") use case, where multiple clients connect to a "VPN Server" and
their traffic is mixed together.  An attacker in this case can record
the traffic and then later compel production of the server's secret
keys, allowing the attacker to link size and timing of information
flows to specific users of the service.

To solve this problem, we introduce a simple key rotation mechanism,
which allows one peer to produce and register ephemeral keys with the
other peer, which can be used as an endpoint for standard (unmodified)
WireGuard connections.

Vocabulary
----------

We refer to the peer that produces the ephemeral keys as the "client"
and the the other peer as the "server".


Outstanding problems
--------------------

Can we guarantee that a given WG key for a given WG interface does not
get reused across multiple underlying interfaces?  If not, a given WG
key might get re-used.

If we can't guarantee forward secrecy, what should we do?  some
forward-secrecy is still better than nothing.
